ARG RUBY_VERSION

FROM ruby:${RUBY_VERSION}-alpine

ARG PRE_INSTALL_BUNDLER_VERSION="~> 2.1"
ARG PRE_INSTALL_RAILS_VERSION="~> 7.0.0"

# install chromedriver if requested
ARG CHROMEDRIVER
RUN if [[ -z ${CHROMEDRIVER+x} ]]; then echo "without chromedriver"; else apk add --no-cache chromium-chromedriver; fi

WORKDIR /app
ENV EDITOR=vim

ONBUILD RUN gem install --no-document bundler -v "${PRE_INSTALL_BUNDLER_VERSION}"
ONBUILD RUN gem install --no-document rails -v "${PRE_INSTALL_RAILS_VERSION}"
RUN gem install --no-document bundler --version "${PRE_INSTALL_BUNDLER_VERSION}"

RUN apk add --no-cache nodejs yarn
RUN apk add --no-cache vim curl mariadb-dev postgresql-dev tzdata build-base

# add less to make pry work
# https://github.com/pry/pry/issues/1248#issuecomment-366056015
RUN apk add --no-cache less
